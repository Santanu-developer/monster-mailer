@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')
<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom"><i class="icon-home"></i> Dashboard</a></div>
  </div>
<!--End-breadcrumbs-->

<!--Action boxes-->
<div class="container-fluid">
    <div class="quick-actions_homepage">
        <ul class="quick-actions">
            <li class="bg_lb span3">
                <a href="{{ route('businessRegistration.index') }}"> 
                  <i class="icon-th-list"></i> Total Listing
                </a> 
                <span class="label label-important">{{ $businessRegistrations->count() }}</span>
            </li>
        </ul>
    </div>

    <div class="row-fluid">
      <div class="span12">
        <div class="widget-box">
          <div class="widget-title bg_ly" data-toggle="collapse" href="#collapseG2"><span class="icon"><i class="icon-chevron-down"></i></span>
            <h5>Latest Business Registrations</h5>
          </div>
          <div class="widget-content nopadding collapse in" id="collapseG2">
            <ul class="recent-posts">
              @foreach($businessRegistrations as $businessRegistration)
              <li>
                <div class="user-thumb"> 
                  <img width="40" height="40" alt="User" src="{{  isset($businessRegistration->image) ? Storage::url($businessRegistration->image) : asset('adminAssets/img/default-org.png') }}"> 
                </div>

                <div class="article-post"> 
                  <span class="user-info"> 
                    <i class="fa fa-user" aria-hidden="true"></i> <b>By:</b> {{ $businessRegistration->name }} | 
                    <i class="fa fa-calendar" aria-hidden="true"></i> <b>Date:</b> {{date('j F, Y', strtotime($businessRegistration->created_at)) }} |
                    <a class="btn btn-primary btn-mini" href="{{ route('businessRegistration.edit', $businessRegistration) }}" title="Fill Detailed Infrmation About Company of {{ $businessRegistration->name }}">
                     <i class="fa fa-info-circle" aria-hidden="true"></i> Fill Detailed Information
                    </a>
                  </span>

                  <p>
                   <i class="fa fa-comment" aria-hidden="true"></i> {{isset($businessRegistration->message) ? $businessRegistration->message : 'No Message Provided'}}
                  </p>

                 {{--  <button class="btn btn-primary btn-mini">Fill Detailed Information</button> --}}
                </div>
              </li>
              @endforeach


              </li>
            </ul>
          </div>
        </div>
    </div>
</div>

</div>
<!--End-Action boxes-->  

@endsection
