<!DOCTYPE html>
<html lang="en">
   
    @include('layouts.partials.head')


    <body>
        @include('layouts.partials.navbar')

        @include('layouts.partials.sidebar')
        <!-- Main Wrapper -->
       <div id="content">
    
            @yield('content')
           
        </div>
        <!-- /Main Wrapper -->

      
        @include('layouts.partials.footer')
        
        @include('sweetalert::alert')

        @include('layouts.partials.scripts')
        
    </body>
</html>