<head>
<title>Monster Mailer: @yield('title')</title>
<meta charset="UTF-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<link rel="stylesheet" href="{{ asset('adminAssets/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('adminAssets/css/bootstrap-responsive.min.css') }}" />
<link rel="stylesheet" href="{{ asset('adminAssets/css/datepicker.css') }}" />
<link rel="stylesheet" href="{{ asset('adminAssets/css/select2.css') }}" />
{{-- <link rel="stylesheet" href="{{ asset('adminAssets/css/fullcalendar.css') }}" /> --}}
<link rel="stylesheet" href="{{ asset('adminAssets/css/matrix-style.css') }}" />
<link rel="stylesheet" href="{{ asset('adminAssets/css/matrix-media.css') }}" />
<link href="{{ asset('adminAssets/font-awesome/css/font-awesome.css') }}" rel="stylesheet" />
<link rel="stylesheet" href="{{ asset('adminAssets/css/jquery.gritter.css') }}" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" integrity="sha512-5A8nwdMOWrSz20fDsjczgUidUBR8liPYU+WymTZP1lmY9G6Oc7HlZv156XqnsgNUzTyMefFTcsFH/tnJE/+xBg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<script src="{{ asset('adminAssets/js/jquery.min.js') }}"></script> 
</head>