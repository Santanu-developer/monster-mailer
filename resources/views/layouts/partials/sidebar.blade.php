<!--sidebar-menu-->
<div id="sidebar">
  <a href="{{ route('home') }}" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>

  <ul>
    <li class="{{ substr(Route::current()->getName(),0,4)=='home' ? 'active' : '' }}">
      <a href="{{ route('home') }}"><i class="icon icon-home"></i> <span>Dashboard</span></a>
    </li>
    

    <li class="submenu {{ substr(Route::current()->getName(),0,6)=='about.' || substr(Route::current()->getName(),0,10)=='advertise.' ? 'active open' : '' }}"> 

      <a href="#"><i class="icon icon-th-list"></i> <span>CMS Management</span></a>
      <ul>
        <li class="{{ substr(Route::current()->getName(),0,6)=='about.' ? 'active' : '' }}">
          <a href="{{ route('about.index') }}">Manage About Us</a>
        </li>

        <li class="{{ substr(Route::current()->getName(),0,10)=='advertise.' ? 'active' : '' }}">
          <a href="{{ route('advertise.index') }}">Manage Advertise</a>
        </li>
      </ul>
    </li>
 
    <li class="submenu {{ substr(Route::current()->getName(),0,21)=='businessRegistration.' || substr(Route::current()->getName(),0,17)=='businessCategory.' ? 'active open' : '' }}"> 
      <a href="#"><i class="icon icon-file"></i> <span>Listing Management</span></a>
      <ul>

        <li class="{{ substr(Route::current()->getName(),0,17)=='businessCategory.' ? 'active' : '' }}">
          <a href="{{ route('businessCategory.index') }}">Manage Business Category</a>
        </li>

        <li class="{{ substr(Route::current()->getName(),0,21)=='businessRegistration.' ? 'active' : '' }}">
          <a href="{{ route('businessRegistration.index') }}">Manage CTA</a>
        </li>
      </ul>
    </li>

    <li class="submenu {{ substr(Route::current()->getName(),0,7)=='coupon.' ? 'active open' : '' }}"> 
      <a href="#"><i class="icon icon-info-sign"></i> <span>Coupon Management</span></a>
      <ul>
        <li class="{{ substr(Route::current()->getName(),0,7)=='coupon.' ? 'active' : '' }}">
          <a href="{{ route('coupon.index') }}">Manage Coupons</a>
        </li>
      </ul>
    </li>

     <li class="submenu {{ substr(Route::current()->getName(),0,5)=='page.' ? 'active open' : '' }}"> 
      <a href="#"><i class="icon icon-file"></i> <span>Page Management</span></a>
      <ul>
        <li class="{{ substr(Route::current()->getName(),0,5)=='page.' ? 'active' : '' }}">
          <a href="{{ route('page.index') }}">Manage Pages</a>
        </li>
      </ul>
    </li>
    
  </ul>
</div>
<!--sidebar-menu-->