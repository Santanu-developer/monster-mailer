<!--Header-part-->
<div id="header">
  <a href="{{ route('home') }}">
    <img src="{{ asset('adminAssets/img/logo.png') }}" alt="Monster_Mailer_Logo" width="3.5%" class="logo-position" />
  </a>
</div>
<!--close-Header-part--> 


<!--top-Header-menu-->
<div id="user-nav" class="navbar navbar-inverse">
  <ul class="nav">
    <li  class="dropdown" id="profile-messages" ><a title="" href="#" data-toggle="dropdown" data-target="#profile-messages" class="dropdown-toggle"><i class="icon icon-user"></i>  <span class="text">Welcome  {{ Auth::user()->name }}</span><b class="caret"></b></a>
      <ul class="dropdown-menu">
        <li>
          <a href="{{ route('showChangePassword') }}"><i class="fa fa-unlock-alt"></i> Change Password</a>
        </li>
        <li class="divider"></li>
        <li>
          <a class="dropdown-item" href="{{ route('logout') }}"
             onclick="event.preventDefault();
                           document.getElementById('logout-form').submit();">
             <i class="icon-key"></i> {{ __('Logout') }}
          </a>

          <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
              @csrf
          </form>

        </li>
      </ul>
    </li>

  </ul>
</div>
<!--close-top-Header-menu-->

