<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>Create Business Category</h5>
  </div>

  <div class="widget-content nopadding">
      <div class="control-group">
        <label class="control-label">Category Name :</label>
        <div class="controls">
          <input type="text" 
                 name="name" 
                 class="span11" 
                 placeholder="Business Catergory Name" 
                 value="{{old('name', isset($businessCategory) ? $businessCategory->name:'')}}" 
                 required />

          @error('name')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

      <div class="form-actions">
        <button type="submit" class="btn btn-success"> {{isset($businessCategory) ? 'Update' : 'Create'}}</button>
        <a href="{{ route('businessCategory.index') }}" class="btn btn-danger">Cancel</a>
      </div>
  </div>
</div>