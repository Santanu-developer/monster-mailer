@extends('layouts.master')

@section('title', 'Edit Business Category')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('businessCategory.index') }}" title="Go to Business Category" class="tip-bottom">
        <i class="icon-home"></i> Edit Business Category</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  
<div class="container-fluid margin-top-2">
    <form action="{{route('businessCategory.update', $businessCategory)}}" method="POST" class="form-horizontal">
      @csrf
      @method('PATCH')
    
      @include('admin.businessCategory.form')
        
    </form>
</div>

@endsection