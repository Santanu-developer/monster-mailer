@extends('layouts.master')

@section('title', 'Business Category')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('businessCategory.index') }}" title="Go to Business Category" class="tip-bottom">
        <i class="icon-home"></i> Business Category</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  <div class="container-fluid">
    <a href="{{ route('businessCategory.create') }}" class="btn btn-success btn-create">Add New Business Category</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Business Categoty List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Category Name</th>
                  <th width="15%">Status</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                @foreach($businessCategories as $businessCategory)
                <tr class="odd gradeX">
                  <td class="align-center">
                      {{ $businessCategory->name }}
                  </td>

                  <td>
                    <div class="dropdown action-label">
                      <a class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                        <?=(isset($businessCategory->status) && $businessCategory->status=='active')?'<i class="fa fa-dot-circle-o text-success"></i> Active':'<i class="fa fa-dot-circle-o text-danger"></i> Inactive';?>

                        <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu">
                          <form action="{{route('businessCategory.destroy', $businessCategory->id)}}" 
                              method="POST" 
                              >
                              @csrf
                              @method('DELETE')
                               <button type="submit" 
                                    class="dropdown-item status-btn" 
                                    style="cursor: pointer;">
                                
                                {!! ($businessCategory->status=='active')? "<i class='fa fa-dot-circle-o text-danger'></i> Inactive":"<i class='fa fa-dot-circle-o text-success'></i> Active" !!}
                              </button>
                          </form>
                        </div>
                    </div>
                  </td>
               
                  <td class="align-center">
                       <a href="{{ route('businessCategory.edit', $businessCategory) }}" class="btn btn-primary">Edit</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection