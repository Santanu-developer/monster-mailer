@extends('layouts.master')

@section('title', 'About Us')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('about.index') }}" title="Go to About" class="tip-bottom">
        <i class="icon-home"></i> About</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  <div class="container-fluid">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>About Us Content</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Short Description</th>
                  <th>Page Content</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                @foreach($abouts as $about)
                <tr class="odd gradeX">
                  <td class="align-center fifteen-width">
                      {{ $about->banner_title }}
                  </td>
                  <td class="align-center thirty-width">
                    {{ $about->short_description }}
                  </td>
                  <td class="thirty-width">
                      {!! $about->content !!}
                  </td>
                  
                  <td class="align-center ten-width">
                       <a href="{{ route('about.edit', $about) }}" class="btn btn-primary">Edit</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection