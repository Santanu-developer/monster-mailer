@extends('layouts.master')

@section('title', 'Edit About Us')

@section('content')

<style>
.ck-editor__editable_inline {
    min-height: 400px;
}

.ck.ck-editor {
    max-width: 67rem;
}
</style>

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('about.index') }}" title="Go to About" class="tip-bottom">
        <i class="icon-home"></i>About</a>
    </div>
  </div>
<!--End-breadcrumbs-->

{{-- {{ dd($about)}} --}}

<div class="container-fluid margin-top-2">
    <form action="{{route('about.update', $about)}}" method="POST" enctype="multipart/form-data" class="form-horizontal">
    @csrf
    @method('PATCH')

    <div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>About Page Content</h5>
      </div>

    <div class="control-group">
        <label class="control-label">Banner Title :</label>
        <div class="controls">
          <input type="text" 
                 name="banner_title" 
                 class="span11" 
                 placeholder="Banner Title" 
                 value="{{old('banner_title', isset($about) ? $about->banner_title:'')}}" 
                 required />

          @error('banner_title')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

    
      <div class="control-group">
        <label class="control-label">Short Description :</label>
        <div class="controls">
          <textarea class="span11" name="short_description" rows="5">{{old('short_description', isset($about) ? $about->short_description:'')}}</textarea>

          @error('short_description')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>


      <div class="control-group">
        <label class="control-label">Banner Image</label>
        <div class="controls">
          <input type="file" name="banner_image" accept="image/*" onchange="showBannerImage(this)" />

          @error('banner_image')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror

         <img src="{{  isset($about->banner_image) ? Storage::url($about->banner_image) : asset('adminAssets/img/default-org.png') }}"
                   id="AboutBannerImg"
                   class="frame frame-1"
                   title="Banner Image"
                   alt="banner_image">
        </div>

      </div>

       <div class="control-group">
        <label class="control-label">Page Content :</label>
        <div class="controls">
            <textarea name="content" id="About" cols="30" rows="10">{{ old("content", $about->content) }}</textarea>

          @error('content')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

     <div class="form-actions">
         <button type="submit" class="btn btn-success margin-top-1">Update</button>
        <a href="{{ route('about.index') }}" class="btn btn-danger margin-top-1">Cancel</a>
      </div>
        
       
    </div>
    </form>
</div>



<script src="{{ asset('adminAssets/plugins/ckeditor5/ckeditor.js') }}"></script>


@include('admin.about-us.scripts')

@endsection