@extends('layouts.master')

@section('title', 'Advertise')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('advertise.index') }}" title="Go to Advertise" class="tip-bottom">
        <i class="icon-home"></i> Advertise</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  <div class="container-fluid">
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Advertise Content</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Short Description</th>
                  <th>Page Content</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                @foreach($advertises as $advertise)
                <tr class="odd gradeX">
                  <td class="align-center fifteen-width">
                      {{ $advertise->banner_title }}
                  </td>
                  <td class="align-center thirty-width">
                    {{ $advertise->short_description }}
                  </td>
                  <td class="thirty-width">
                      {!! $advertise->content !!}
                  </td>
                  
                  <td class="align-center ten-width">
                       <a href="{{ route('advertise.edit', $advertise) }}" class="btn btn-primary">Edit</a>
                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection