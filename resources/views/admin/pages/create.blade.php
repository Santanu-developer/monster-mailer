@extends('layouts.master')

@section('title', 'Create Page')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('page.index') }}" title="Go to Page" class="tip-bottom">
        <i class="icon-home"></i> Create Page</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  
<div class="container-fluid margin-top-2">
    <form action="{{route('page.store')}}" method="POST" class="form-horizontal" autocomplete="off" enctype="multipart/form-data">
    @csrf
    
      @include('admin.pages.form')
        
    </form>
</div>

@endsection