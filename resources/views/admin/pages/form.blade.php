<style>
.ck-editor__editable_inline {
    min-height: 400px;
}

.ck.ck-editor {
    max-width: 67rem;
}
</style>

<div class="widget-box">
      <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
        <h5>Page SEO setup</h5>
      </div>

        <div class="control-group">
          <label class="control-label">Meta Title :</label>
          <div class="controls">
            <input type="text" 
                   name="meta_title" 
                   class="span11" 
                   placeholder="Meta Title" 
                   value="{{old('meta_title', isset($page) ? $page->meta_title:'')}}" 
                   required />

            @error('meta_title')
              <p class="alert alert-error custom-error">
                <strong>{{ $message }}</strong>
              </p>
            @enderror
          </div>
        </div>

      <div class="control-group">
        <label class="control-label">Meta Keyword :</label>
        <div class="controls">
          <textarea class="span11" name="meta_keyword" rows="4" placeholder="Meta Keyword">{{old('meta_keyword', isset($page) ? $page->meta_keyword:'')}}</textarea>

          @error('meta_keyword')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>


      <div class="control-group">
        <label class="control-label">Meta Description :</label>
        <div class="controls">
          <textarea class="span11" name="meta_description" rows="5" placeholder="Meta Description">{{old('meta_description', isset($page) ? $page->meta_description:'')}}</textarea>

          @error('meta_description')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>



  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>Page Details</h5>
  </div>


  <div class="widget-content nopadding">
      <div class="control-group">
        <label class="control-label">Page Name :</label>
        <div class="controls">
          <input type="text" 
                 name="name" 
                 class="span11 uppercese" 
                 placeholder="Page Name" 
                 value="{{old('name', isset($page) ? $page->name:'')}}" 
                 required />

          @error('name')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

      <div class="control-group">
        <label class="control-label">Banner Title :</label>
        <div class="controls">
          <input type="text" 
                 name="banner_title" 
                 class="span11" 
                 placeholder="Banner Title" 
                 value="{{old('banner_title', isset($page) ? $page->banner_title:'')}}" 
                 required />

          @error('banner_title')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

    
      <div class="control-group">
        <label class="control-label">Short Description :</label>
        <div class="controls">
          <textarea class="span11" name="short_description" rows="5">{{old('short_description', isset($page) ? $page->short_description:'')}}</textarea>

          @error('short_description')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>


      <div class="control-group">
        <label class="control-label">Banner Image</label>
        <div class="controls">
          <input type="file" name="banner_image" accept="image/*" onchange="showBannerImage(this)" />

          @error('banner_image')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror

         <img src="{{  isset($page->banner_image) ? Storage::url($page->banner_image) : asset('adminAssets/img/default-org.png') }}"
                   id="BannerImg"
                   class="frame frame-1"
                   title="Banner Image"
                   alt="banner_image">
        </div>


      </div>

     

      <div class="control-group">
        <label class="control-label">Page Content :</label>
        <div class="controls">
          <textarea class="span11" name="content" id="PageContent" rows="4">{{old('content', isset($page) ? $page->content:'')}}</textarea>

          @error('content')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>


      <div class="form-actions">
        <button type="submit" class="btn btn-success"> {{isset($page) ? 'Update' : 'Create'}}</button>
        <a href="{{ route('page.index') }}" class="btn btn-danger">Cancel</a>
      </div>
  </div>
</div>



<script src="{{ asset('adminAssets/plugins/ckeditor5/ckeditor.js') }}"></script>
<script src="{{ asset('adminAssets/js/custom/page.js') }}"></script>

@include('admin.pages.scripts')