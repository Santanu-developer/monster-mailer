@extends('layouts.master')

@section('title', 'Edit Page')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('page.index') }}" title="Go to Page" class="tip-bottom">
        <i class="icon-home"></i> Edit Page</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  
<div class="container-fluid margin-top-2">
    <form action="{{route('page.update', $page)}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
      @csrf
      @method('PATCH')
    
      @include('admin.pages.form')
        
    </form>
</div>

@endsection