@extends('layouts.master')

@section('title', 'Page Listing')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('page.index') }}" title="Go to Page Listing" class="tip-bottom">
        <i class="icon-home"></i> Page Listing</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  <div class="container-fluid">
    <a href="{{ route('page.create') }}" class="btn btn-success btn-create">Create New Page</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Page List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Title</th>
                  <th>Meta Title</th>
                  <th width="15%">Status</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                @if($pages->count() > 0 )
                  @foreach($pages as $page)
                    <tr class="odd gradeX">
                      <td class="align-center">
                          {{ $page->name }}
                      </td>
                      <td class="align-center">
                          {{ $page->meta_title }}
                      </td>
                    
                       <td>
                        <div class="dropdown action-label">
                          <a class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                            <?=(isset($page->status) && $page->status=='active')?'<i class="fa fa-dot-circle-o text-success"></i> Active':'<i class="fa fa-dot-circle-o text-danger"></i> Inactive';?>

                            <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu">
                              <form action="{{route('page.destroy', $page->id)}}" 
                                  method="POST" 
                                  >
                                  @csrf
                                  @method('DELETE')
                                   <button type="submit" 
                                        class="dropdown-item status-btn" 
                                        style="cursor: pointer;">
                                    
                                    {!! ($page->status=='active')? "<i class='fa fa-dot-circle-o text-danger'></i> Inactive":"<i class='fa fa-dot-circle-o text-success'></i> Active" !!}
                                  </button>
                              </form>
                            </div>
                        </div>
                      </td>
                      
                      <td class="align-center">
                           <a href="{{ route('page.edit', $page) }}" class="btn btn-primary">Edit</a>
                      </td>
                    </tr>
                  @endforeach
                @else
                  <td>No Pages found</td>
                @endif
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection