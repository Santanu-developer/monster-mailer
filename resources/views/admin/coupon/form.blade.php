<style>
.ck-editor__editable_inline {
    min-height: 300px;
}

.ck.ck-editor {
    max-width: 67rem;
}
</style>

<div class="widget-box">
  <div class="widget-title"> <span class="icon"> <i class="icon-align-justify"></i> </span>
    <h5>{{isset($coupon) ? 'Edit' : 'Create'}} Coupon</h5>
  </div>


  <div class="control-group">
    <label class="control-label">Choose Business</label>
    <div class="controls">
      <select class="business" name="business">
        <option value="">Select</option>
        @foreach($businessRegistrations as $businessRegistration)
          <option value="{{ $businessRegistration->id }}"@if(old('business', isset($coupon) ? $coupon->business->id:-1)==$businessRegistration->id ) selected='selected' @endif>
            {{ $businessRegistration->company_name }}
          </option>
        @endforeach
      </select>

      @error('business')
        <p class="alert alert-error custom-error">
          <strong>{{ $message }}</strong>
        </p>
      @enderror
    </div>
  </div>


  <div class="widget-content nopadding">
      <div class="control-group">
        <label class="control-label">Coupon Title :</label>
        <div class="controls">
          <input type="text" 
                 name="title" 
                 class="span11" 
                 placeholder="Business Catergory Name" 
                 value="{{old('title', isset($coupon) ? $coupon->title:'')}}" 
                 required />

          @error('title')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

      <div class="control-group">
        <label class="control-label"> Coupon Expiry Date :</label>
        <div class="controls">
           <input type="text" value="{{old('expiry', isset($coupon) ? $coupon->expiry:'')}}" class="datepicker span11" name="expiry">
            <span class="help-block">Date with Formate of (yyyy-mm-dd)</span> 

          @error('expiry')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

      <div class="control-group">
        <label class="control-label">Short Description :</label>
        <div class="controls">
          <textarea class="span11" name="short_description" rows="5">{{old('short_description', isset($coupon) ? $coupon->short_description:'')}}</textarea>

          @error('short_description')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>

     

      <div class="control-group">
        <label class="control-label">Long Description :</label>
        <div class="controls">
          <textarea class="span11" name="description" id="CouponDescription" rows="4">{{old('description', isset($coupon) ? $coupon->description:'')}}</textarea>

          @error('description')
            <p class="alert alert-error custom-error">
              <strong>{{ $message }}</strong>
            </p>
          @enderror
        </div>
      </div>



      <div class="form-actions">
        <button type="submit" class="btn btn-success"> {{isset($coupon) ? 'Update' : 'Create'}}</button>
        <a href="{{ route('coupon.index') }}" class="btn btn-danger">Cancel</a>
      </div>
  </div>
</div>


<script src="{{ asset('adminAssets/plugins/ckeditor5/ckeditor.js') }}"></script>
<script src="{{ asset('adminAssets/js/custom/coupon.js') }}"></script>