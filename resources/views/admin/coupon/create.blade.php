@extends('layouts.master')

@section('title', 'Create Coupon')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('coupon.index') }}" title="Go to Coupon" class="tip-bottom">
        <i class="icon-home"></i> Create Coupon</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  
<div class="container-fluid margin-top-2">
    <form action="{{route('coupon.store')}}" method="POST" class="form-horizontal" autocomplete="off">
    @csrf
    
      @include('admin.coupon.form')
        
    </form>
</div>

@endsection