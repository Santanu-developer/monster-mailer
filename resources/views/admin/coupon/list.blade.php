@extends('layouts.master')

@section('title', 'Coupon Listing')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('coupon.index') }}" title="Go to Coupon Listing" class="tip-bottom">
        <i class="icon-home"></i> Coupon Listing</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  <div class="container-fluid">
    <a href="{{ route('coupon.create') }}" class="btn btn-success btn-create">Create New Coupon</a>
        <div class="widget-box">
          <div class="widget-title"> <span class="icon"> <i class="icon-th"></i> </span>
            <h5>Coupon List</h5>
          </div>
          <div class="widget-content nopadding">
            <table class="table table-bordered table-striped">
              <thead>
                <tr>
                  <th>Business Name</th>
                  <th>Title</th>
                  <th>Expiry Date</th>
                  <th>Coupon Code</th>
                  <th width="15%">Status</th>
                  <th>Action</th>
                </tr>
              </thead>

              <tbody>
                @if($coupons->count() > 0 )
                  @foreach($coupons as $coupon)
                    <tr class="odd gradeX">
                      <td class="align-center">
                          {{ $coupon->business->company_name }}
                      </td>
                      <td class="align-center">
                          {{ $coupon->title }}
                      </td>
                      <td class="align-center">
                          {{ $coupon->expiry }}
                      </td>
                      <td class="align-center">
                        <strong>
                          <span style="color: red">{{ $coupon->coupon_code }}</span>
                        </strong>
                      </td>

                       <td>
                        <div class="dropdown action-label">
                          <a class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                            <?=(isset($coupon->status) && $coupon->status=='active')?'<i class="fa fa-dot-circle-o text-success"></i> Active':'<i class="fa fa-dot-circle-o text-danger"></i> Inactive';?>

                            <span class="caret"></span>
                            </a>
                            <div class="dropdown-menu">
                              <form action="{{route('coupon.destroy', $coupon->id)}}" 
                                  method="POST" 
                                  >
                                  @csrf
                                  @method('DELETE')
                                   <button type="submit" 
                                        class="dropdown-item status-btn" 
                                        style="cursor: pointer;">
                                    
                                    {!! ($coupon->status=='active')? "<i class='fa fa-dot-circle-o text-danger'></i> Inactive":"<i class='fa fa-dot-circle-o text-success'></i> Active" !!}
                                  </button>
                              </form>
                            </div>
                        </div>
                      </td>
                      
                      <td class="align-center">
                           <a href="{{ route('coupon.edit', $coupon) }}" class="btn btn-primary">Edit</a>
                      </td>
                    </tr>
                  @endforeach
                @else
                  <td>No Coupons Listed</td>
                @endif
              </tbody>
            </table>
          </div>
        </div>
    </div>

@endsection