@extends('layouts.master')

@section('title', 'Edit Coupon')

@section('content')

<!--breadcrumbs-->
  <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('coupon.index') }}" title="Go to Coupon" class="tip-bottom">
        <i class="icon-home"></i> Edit Coupon</a>
    </div>
  </div>
<!--End-breadcrumbs-->

  
<div class="container-fluid margin-top-2">
    <form action="{{route('coupon.update', $coupon)}}" method="POST" class="form-horizontal">
      @csrf
      @method('PATCH')
    
      @include('admin.coupon.form')
        
    </form>
</div>

@endsection