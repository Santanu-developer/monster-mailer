@extends('layouts.master')

@section('title', 'Dashboard')

@section('content')
<!--breadcrumbs-->
   <div id="content-header">
    <div id="breadcrumb"> 
        <a href="{{ route('home') }}" title="Go to Dashboard" class="tip-bottom">
            <i class="icon-home"></i> Dashboard</a>
        <a href="{{ route('showChangePassword') }}" title="Go to Password Change" class="tip-bottom">
        <i class="icon-home"></i> Change Password</a>
    </div>
  </div>
<!--End-breadcrumbs-->

<div class="container-fluid margin-top-2">
    <form action="{{route('changeDefaultPassword')}}" method="POST" class="form-horizontal" autocomplete="off">
        @csrf

        <div class="control-group">
          <label class="control-label"><i class="fa fa-unlock-alt"></i> Current Password :</label>
          <div class="controls">
              <input type="password" 
                     placeholder="Current Password"  
                     name="current"
                     required>

            @error('current')
              <p class="alert alert-error custom-error">
                <strong>{{ $message }}</strong>
              </p>
            @enderror
          </div>
        </div>

        <div class="control-group">
          <label class="control-label"><i class="fa fa-unlock-alt"></i> New Password :</label>
          <div class="controls">
              <input type="password" placeholder="New Password" name="password" required>

            @error('password')
              <p class="alert alert-error custom-error">
                <strong>{{ $message }}</strong>
              </p>
            @enderror
          </div>
        </div>

        <div class="control-group">
          <label class="control-label"><i class="fa fa-unlock-alt"></i> Confirm Password :</label>
          <div class="controls">
            <input type="password" placeholder="Confirm New Password" name="password_confirmation">

            @error('password_confirmation')
              <p class="alert alert-error custom-error">
                <strong>{{ $message }}</strong>
              </p>
            @enderror
          </div>
        </div>

    
      <div class="form-actions">
        <button type="submit" class="btn btn-success"> Change Password</button>
        <a href="{{ route('home') }}" class="btn btn-danger">Cancel</a>
      </div>
        
    </form>
</div>



@endsection