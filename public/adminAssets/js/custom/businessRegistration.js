  ClassicEditor
    .create( document.querySelector( '#OpeningHours' ), {

     toolbar: {
          items: [
              'heading', '|',
              'bold', 'italic', '|',
              'link', '|',
              'outdent', 'indent', '|',
              'bulletedList', 'numberedList', '|',
              'insertTable', '|',
              'blockQuote', '|',
              'undo', 'redo'
          ],
          shouldNotGroupWhenFull: true
      }

      }).catch( error => {
          console.error( error );
      });


  ClassicEditor
      .create( document.querySelector( '#Description' ), {

      toolbar: {
            items: [
                'heading', '|',
                'bold', 'italic', '|',
                'link', '|',
                'outdent', 'indent', '|',
                'bulletedList', 'numberedList', '|',
                'insertTable', '|',
                'blockQuote', '|',
                'undo', 'redo'
            ],
            shouldNotGroupWhenFull: true
        }
      }).catch( error => {
          console.error( error );
      });






$(document).ready(function(){
    $('.datepicker').datepicker({
       format: 'yyyy-mm-dd',


     }).on('changeDate', function(e){
          $(this).datepicker('hide');
    }); 


    $('.business-category').select2({
        width : '74%'
      });
  });


  function showImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        
        reader.onload = function (e) {
            $('#UploadedImg').attr('src', e.target.result);
        }
        reader.readAsDataURL(input.files[0]);
    }
}