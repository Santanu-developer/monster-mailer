  ClassicEditor
    .create( document.querySelector( '#CouponDescription' ), {

     toolbar: {
          items: [
              'heading', '|',
              'bold', 'italic', '|',
              'link', '|',
              'outdent', 'indent', '|',
              'bulletedList', 'numberedList', '|',
              'insertTable', '|',
              'blockQuote', '|',
              'undo', 'redo'
          ],
          shouldNotGroupWhenFull: true
      }

      }).catch( error => {
          console.error( error );
      });



$(document).ready(function(){
    $('.datepicker').datepicker({
       format: 'yyyy-mm-dd',


     }).on('changeDate', function(e){
          $(this).datepicker('hide');
    }); 


    $('.business').select2({
        width : '74%'
      });
  });