<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.index');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');

//Removing/Avoiding Register & Password Reset functionality 
Route::get('/register', function() {
    return redirect('/login');
});

Route::post('/register', function() {
    return redirect('/login');
});

Route::get('/password/reset', function() {
    return redirect('/login');
});

Route::post('/password/reset', function() {
    return redirect('/login');
});



Route::prefix('admin')->group(function () {

    Route::resource('about', 'AboutController', [
        'names' => 'about',
        'except' => [
            'show',
            'create',
            'destroy'
        ]
    ]);

    Route::resource('advertise', 'AdvertiseController', [
        'names' => 'advertise',
        'except' => [
            'show',
            'create',
            'destroy'
        ]
    ]);

    Route::resource('businessRegistration', 'BusinessRegistrationController', [
        'names' => 'businessRegistration',
        'except' => [
            'show',
        ]
    ]);

    Route::resource('businessCategory', 'BusinessCategoryController', [
        'names' => 'businessCategory',
        'except' => [
            'show',
        ]
    ]);


    Route::resource('coupon', 'CouponController', [
        'names' => 'coupon',
        'except' => [
            'show',
        ]
    ]);


     Route::resource('page', 'PageController', [
        'names' => 'page',
        'except' => [
            'show',
        ]
    ]);


    Route::post('imageUploadCK', 'PageController@imageUploadCK')->name('imageUploadCK');


    // Route for Show Change Password form and change user Password
    Route::get('changePassword', 'HomeController@showChangePassword')->name('showChangePassword');
    Route::post('changePassword', 'HomeController@changePassword')->name('changeDefaultPassword');
});