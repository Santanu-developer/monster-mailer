<?php

namespace App\Http\Controllers;

use App\Models\BusinessCategory;
use Illuminate\Http\Request;

class BusinessCategoryController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['businessCategories'] = BusinessCategory::all();
        return view('admin.businessCategory.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('admin.businessCategory.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $values = $request->validate([
            'name' => 'required|string|max:150|unique:business_categories,name'
        ]);

        $businessCategory = new BusinessCategory();
        $businessCategory->fill($values);
        $businessCategory->save();

        toast($businessCategory->name . ' Created Successfully!','success');

        return redirect()->route('businessCategory.index');
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BusinessCategory  $businessCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessCategory $businessCategory)
    {
        $data['businessCategory'] = $businessCategory;
        return view('admin.businessCategory.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\BusinessCategory  $businessCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessCategory $businessCategory)
    {
        $changed = false;
        $values = $request->validate([
            'name' => 'required|string|max:150|unique:business_categories,name,' . $businessCategory->id
        ]);

        $businessCategory->fill($values);
        if ($businessCategory->isDirty()) {
            $changed = true;
            $businessCategory->save();
        }

        if (! $changed) {
            toast('No changes done to save','warning');
            return redirect()->route('businessCategory.index')->withErrors('No changes done to save');
        }

        toast($businessCategory->name . ' Updated Successfully!','success');
        return redirect()->route('businessCategory.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BusinessCategory  $businessCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessCategory $businessCategory)
    {
        if ($businessCategory->status == 'active') {
            $businessCategory->status = 'inactive';
        } else {
            $businessCategory->status = 'active';
        }

        $businessCategory->save();
        
        toast($businessCategory->name . ' ' . (($businessCategory->status == 'inactive') ? 'Disabled' : 'Enabled') . ' successfully!','success');

        return redirect()->route('businessCategory.index');
    }
}
