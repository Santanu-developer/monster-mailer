<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\BusinessRegistration;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Validation\ValidationException;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data['businessRegistrations'] = BusinessRegistration::orderBy('created_at', 'desc')->get();
        return view('home', $data);
    }



    public function showChangePassword() {
        return view('auth.passwords.changePassword');
    }


    public function changePassword(Request $request) {

        $value =  $request->validate([
            'current' => ['required', 'string', function ($attribute, $value, $fail) {
                if (!Hash::check($value, Auth::user()->password)) {
                    return $fail(__('The current password is incorrect.'));
                }
            }],
            'password' => 'required|string|confirmed|min:4|max:20|different:current',
        ]);

        if(Auth::guard()->attempt(['name'=>Auth::user()->name, 'password' => $value['current']])){
            $user = Auth::user();
            $user->password = Hash::make($value['password']);
            $user->save();

            toast('Your password has been successfully changed!','success');
            return redirect()->route('home');
        }
        throw ValidationException::withMessages([
            'current' => [trans('auth.failed')],
        ]);
    }

}
