<?php

namespace App\Http\Controllers;
use App\Models\About;
Use Alert;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

use Illuminate\Http\Request;

class AboutController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['abouts'] = About::all();
        return view('admin.about-us.list', $data);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(About $about)
    {
        // dd($about);
        $data['about'] = $about;
        return view('admin.about-us.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, About $about)
    {
        $changed = false;
        $values = $request->validate([
            "banner_title" => 'required|string|max:100',
            "short_description"=>'nullable|string|max:200',
            "banner_image"=>'nullable|image|max:3000',
            'content' => 'required|string|max:5000'
        ]);

        //dd($values['banner_title'], $values['short_description']);

        if ($request->banner_image) {
            Storage::delete($about->banner_image);
            $values['banner_image'] = Storage::putFile('public/about', new File($request->banner_image));
        }

        $about->fill($request->except('banner_image'));
        if (isset($values['banner_image'])) {
            $about->banner_image = $values['banner_image'];
        }

        if ($about->isDirty()) {
            $changed = true;
            $about->save();
        }

        if (! $changed) {
            toast('No changes done to save','warning');
            return redirect()->route('about.index')->withErrors('No changes done to save');
        }
        
        toast('About Info updated successsfully!','success');

        return redirect()->route('about.index');
       
    }



    public function store(Request $request)
    {
        $about = new About();
        $about->id = 0;
        $about->exists = true;
        $image = $about->addMediaFromRequest('upload')->toMediaCollection('images');

       return response()->json([
        'url'=> $image->getUrl('thumb')
       ]);
    }

  
}
