<?php

namespace App\Http\Controllers;

use App\Models\Advertise;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class AdvertiseController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['advertises'] = Advertise::all();
        return view('admin.advertise.list', $data);
    }



    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertise $advertise)
    {
        $data['advertise'] = $advertise;
        return view('admin.advertise.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Advertise  $advertise
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Advertise $advertise)
    {
        $changed = false;
        $values = $request->validate([
            "banner_title" => 'required|string|max:100',
            "short_description"=>'nullable|string|max:200',
            "banner_image"=>'nullable|image|max:3000',
            'content' => 'required|string|max:5000'
        ]);

        if ($request->banner_image) {
            Storage::delete($advertise->banner_image);
            $values['banner_image'] = Storage::putFile('public/advertise', new File($request->banner_image));
        }

        $advertise->fill($request->except('banner_image'));
        
        if (isset($values['banner_image'])) {
            $advertise->banner_image = $values['banner_image'];
        }

        if ($advertise->isDirty()) {
            $changed = true;
            $advertise->save();
        }

        if (! $changed) {
            toast('No changes done to save','warning');
            return redirect()->route('advertise.index')->withErrors('No changes done to save');
        }
        
        toast('Advertise Info updated successsfully!','success');

        return redirect()->route('advertise.index');
    }



    public function store(Request $request)
    {
        $advertise = new Advertise();
        $advertise->id = 0;
        $advertise->exists = true;
        $image = $advertise->addMediaFromRequest('upload')->toMediaCollection('images');

       return response()->json([
        'url'=> $image->getUrl('thumb')
       ]);
    }

    
}
