<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;

class PageController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['pages'] = Page::all();
        return view('admin.pages.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $values = $request->validate([
            "name" => 'required|string|max:50|unique:pages,name',
            "banner_title" => 'required|string|max:100',
            "content"=>'required|string',
            "short_description"=>'nullable|string|max:200',
            "banner_image"=>'nullable|image|max:3000',
            "meta_title"=>'required|string|max:100',
            "meta_keyword"=>'nullable|string|max:200',
            "meta_description"=>'nullable|string',
        ]);
        

        if (isset($values['banner_image'])) {
            $values['banner_image'] = Storage::putFile('public/pages', new File($request->banner_image));
        }

        $page = new Page();
        $page->fill($values);
        $page->save();

        toast($page->name . ' created Successfully!','success');

        return redirect()->route('page.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $data['page'] = $page;
        return view('admin.pages.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Page $page)
    {
        $changed = false;
        $values = $request->validate([
            "name" => 'required|string|max:50|unique:pages,name,'. $page->id,
            "banner_title" => 'required|string|max:100',
            "content"=>'required|string',
            "short_description"=>'nullable|string|max:200',
            "banner_image"=>'nullable|image|max:3000',
            "meta_title"=>'required|string|max:100',
            "meta_keyword"=>'nullable|string|max:200',
            "meta_description"=>'nullable|string',
        ]);

        if ($request->banner_image) {
            Storage::delete($page->banner_image);
            $values['banner_image'] = Storage::putFile('public/pages', new File($request->banner_image));
        }

        $page->fill($request->except('banner_image'));
        if (isset($values['banner_image'])) {
            $page->banner_image = $values['banner_image'];
        }

        if ($page->isDirty()) {
            $page->save();
            $changed = true;
        }


        if (! $changed) {
            toast('No changes done to save','warning');
            return redirect()->route('page.index')->withErrors('No changes done to save');
        }

        toast($page->name . ' updated successsfully!','success');
        return redirect()->route('page.index');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page  $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        if ($page->status == 'active') {
            $page->status = 'inactive';
        } else {
            $page->status = 'active';
        }

        $page->save();
        
        toast($page->name . ' ' . (($page->status == 'inactive') ? 'Disabled' : 'Enabled') . ' successfully!','success');
        return redirect()->route('page.index');
    }



    public function imageUploadCK(Request $request)
    {
        $page = new Page();
        $page->id = 0;
        $page->exists = true;
        $image = $page->addMediaFromRequest('upload')->toMediaCollection('images');

       return response()->json([
        'url'=> $image->getUrl('thumb')
       ]);
    }
}
