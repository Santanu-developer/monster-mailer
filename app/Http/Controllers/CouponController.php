<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Models\BusinessRegistration;

class CouponController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['coupons'] = Coupon::all();
        return view('admin.coupon.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['businessRegistrations'] = BusinessRegistration::where('status','active')->get();
        return view('admin.coupon.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $values = $request->validate([
            "title" => 'required|string|max:100',
            "short_description" => 'required|string|max:250',
            "description" => 'nullable|string|max:500',
            "expiry" => 'required|date|after_or_equal:now',
            "business" => 'required|integer|exists:business_registrations,id',
        ]);

        $business = BusinessRegistration::find($values['business']);
        $couponCode = random_int(100000, 999999);

        //dd($business, $couponCode);

        $coupon = new Coupon();
        $coupon->fill($values);
        $coupon->business()->associate($business);
        $coupon->coupon_code = $couponCode;
        $coupon->save();

        toast($coupon->title . ' added Successfully to!'. $business->company_name,'success');

        return redirect()->route('coupon.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function show(Coupon $coupon)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function edit(Coupon $coupon)
    {
        $data['coupon'] = $coupon;
        $data['businessRegistrations'] = BusinessRegistration::where('status','active')->get();
        return view('admin.coupon.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Coupon $coupon)
    {
        $changed = false;
        $values = $request->validate([
           "title" => 'required|string|max:100',
           "short_description" => 'required|string|max:250',
           "description" => 'nullable|string|max:500',
           "expiry" => 'required|date|after_or_equal:now',
           "business" => 'required|integer|exists:business_registrations,id',
        ]);

      
        $coupon->fill($request->except('business'));
        if ($coupon->isDirty()) {
            $coupon->save();
            $changed = true;
        }


        $business = BusinessRegistration::find($values['business']);
        if ($business != $coupon->business) {
            $coupon->business()->associate($business);
            $coupon->save();
            $changed = true;
        }

        if (! $changed) {
            toast('No changes done to save','warning');
            return redirect()->route('coupon.index')->withErrors('No changes done to save');
        }

        toast($coupon->title . ' updated successsfully!','success');
        return redirect()->route('coupon.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Coupon  $coupon
     * @return \Illuminate\Http\Response
     */
    public function destroy(Coupon $coupon)
    {
        if ($coupon->status == 'active') {
            $coupon->status = 'inactive';
        } else {
            $coupon->status = 'active';
        }

        $coupon->save();
        
        toast($coupon->title . ' ' . (($coupon->status == 'inactive') ? 'Disabled' : 'Enabled') . ' successfully!','success');

        return redirect()->route('coupon.index');
    }
}
