<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessCategory extends Model
{
    use HasFactory;

    protected $fillable = ['name','status'];


    public function businessRegistrations()
    {
        return $this->hasMany(BusinessRegistration::class);
    }
}
