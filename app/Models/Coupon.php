<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'short_description',
        'description',
        'expiry',
        'coupon_code',
        'status'
    ];

    public function business()
    {
        return $this->belongsTo(BusinessRegistration::class, 'business_registration_id');
    }
}
