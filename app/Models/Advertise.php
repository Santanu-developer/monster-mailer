<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Advertise extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

   protected $fillable = [
        'banner_title',
        'short_description',
        'banner_image',
        'content'
    ];


    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
              ->width(500)
              ->sharpen(10);
    }
}
